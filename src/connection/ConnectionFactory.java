package connection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    private static final String URL = "jdbc:mysql://localhost:3306/projeto";
    private static final String USER = "root";
    private static final String PASSWORD = "";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection() {

        Connection connection = null;

        try {
            // careegando o driver do banco 
            Class.forName(DRIVER);
            // Abrir conexao com o banco 
            connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Conectou !!!!!");

        } catch (ClassNotFoundException ex) {
            System.out.println("Erro Driver do banco.");
            ex.printStackTrace();

        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Erro no banco");
        }

        return connection;
    }

   
}